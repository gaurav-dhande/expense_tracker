package utils

import (
	"encoding/json"
	"net/http"
)

type ErrorResponse struct {
	Type    string      `json:"type"`
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Errors  interface{} `json:"errors,omitempty"`
}

func JSONError(w http.ResponseWriter, err interface{}, statusCode int) {
	errorResponse := ErrorResponse{}
	errorResponse.Type = "invalid_request_error"
	errorResponse.Code = statusCode

	if statusCode == http.StatusBadRequest {
		errorResponse.Message = "One or more required parameters are missing or invalid"
		errorResponse.Errors = err
	} else if statusCode == http.StatusNotFound {
		errorResponse.Message = "The specified resource was not found"
	} else if statusCode == http.StatusInternalServerError {
		errorResponse.Message = "something went wrong"
	} else if statusCode == http.StatusUnauthorized {
		errorResponse.Message = "you are not authorized to perform this action"
	}

	// Write status code to header
	w.WriteHeader(statusCode)
	_ = json.NewEncoder(w).Encode(errorResponse)
}
