package utils

import (
	"fmt"
	"github.com/gaurav/expense_tracker/cmd"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"log"
)

func ConnectToDatabase() {
	db, err := sqlx.Connect("mysql", "root:root@123@(localhost:3306)/EXPENSE_TRACKER")
	if err != nil {
		log.Fatalln(err)
	}

	cmd.DB = db
}

func CreateTables() {
	schemaTransaction := `CREATE TABLE TRANSACTIONS
					(ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
					UUID VARCHAR (255) NOT NULL,
					ACCOUNT_NUMBER INT NOT NULL,
					TRANSACTION_ID VARCHAR (255),
					TRANSACTION_TYPE VARCHAR (255) NOT NULL,
					TRANSACTION_TIME VARCHAR (255) NOT NULL,
					UPDATED_TIME VARCHAR (255) NOT NULL,
					FROM_TO VARCHAR (255) NOT NULL,
					AMOUNT INT NOT NULL
					);`

	schemaAccount := `CREATE TABLE ACCOUNTS	
					(ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
					UUID VARCHAR (255) NOT NULL,
					ACCOUNT_NUMBER INT NOT NULL,
					ACCOUNT_NAME VARCHAR (20) NOT NULL,
					ACCOUNT_TYPE VARCHAR (20) NOT NULL,
					CURRENT_BALANCE INT NOT NULL,
					IS_RED INT NOT NULL);`

	cmd.DB.MustExec(schemaTransaction)
	cmd.DB.MustExec(schemaAccount)

	fmt.Println("Schema created")
}

func DropTables() {
	dropQuery := "DROP TABLE ACCOUNTS"
	cmd.DB.Exec(dropQuery)

	dropQuery = "DROP TABLE TRANSACTIONS"
	cmd.DB.Exec(dropQuery)
}
