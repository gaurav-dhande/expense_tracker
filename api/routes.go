package api

import (
	"github.com/gaurav/expense_tracker/api/controllers"
	"github.com/gorilla/mux"
)

func Routes(router *mux.Router) {
	router.HandleFunc("/api/transactions/{accountId}", controllers.GetAllTransactions).Methods("GET")
	router.HandleFunc("/api/transactions", controllers.AddNewTransaction).Methods("POST")
	router.HandleFunc("/api/transactions/{uuid}", controllers.UpdateTransaction).Methods("PUT")
	router.HandleFunc("/api/accounts", controllers.AddNewAccount).Methods("POST")
	router.HandleFunc("/api/accounts", controllers.GetAllAccounts).Methods("GET")
	router.HandleFunc("/api/accounts/{uuid}", controllers.DeleteAccount).Methods("DELETE")
}
