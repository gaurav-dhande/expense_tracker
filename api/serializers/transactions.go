package serializers

import "github.com/gaurav/expense_tracker/api/models"

type TransactionSerializer struct {
	Transactions []*models.Transaction
	Many         bool
}

func (t *TransactionSerializer) Serialize() map[string]interface{} {
	serialzedData := make(map[string]interface{})

	transactionArray := make([]interface{}, 0)
	for _, transaction := range t.Transactions {
		transactionArray = append(transactionArray, map[string]interface{}{
			"uuid":             transaction.UUID,
			"account_number":   transaction.AccountNumber,
			"transaction_id":   transaction.TransactionId,
			"transaction_type": transaction.TransactionType,
			"transaction_time": transaction.TransactionTime,
			"updated_time":     transaction.UpdatedTime,
			"from_to":          transaction.FromTo,
			"amount":           transaction.Amount,
		})
	}
	if t.Many {
		serialzedData["data"] = transactionArray
	} else {
		if len(transactionArray) != 0 {
			serialzedData["data"] = transactionArray[0]
		} else {
			serialzedData["data"] = make(map[string]interface{})
		}
	}
	return serialzedData
}
