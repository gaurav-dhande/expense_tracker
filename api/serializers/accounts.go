package serializers

import "github.com/gaurav/expense_tracker/api/models"

type AccountSerializer struct {
	Accounts []*models.Account
	Many     bool
}

func (a *AccountSerializer) Serialize() map[string]interface{} {
	serializedData := make(map[string]interface{})

	accountsArray := make([]interface{}, 0)
	for _, account := range a.Accounts {
		accountsArray = append(accountsArray, map[string]interface{}{
			"uuid":            account.UUID,
			"account_number":  account.AccountNumber,
			"account_name":    account.AccountName,
			"account_type":    account.AccountType,
			"current_balance": account.CurrentBalance,
			"is_red":          account.IsRed,
		})
	}

	if a.Many {
		serializedData["data"] = accountsArray
	} else {
		if len(accountsArray) != 0 {
			serializedData["data"] = accountsArray[0]
		} else {
			serializedData["data"] = make(map[string]interface{})
		}
	}

	return serializedData
}
