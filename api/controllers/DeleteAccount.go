package controllers

import (
	"encoding/json"
	"github.com/gaurav/expense_tracker/api/models"
	"github.com/gaurav/expense_tracker/utils"
	"github.com/gorilla/mux"
	"net/http"
)

func DeleteAccount(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	parameters := mux.Vars(r)
	uuid, _ := parameters["uuid"]

	accountInstance := models.Account{}
	errorMap := make(map[string]interface{})

	deletedAccount, err := accountInstance.RetrieveByUUID(uuid)
	if err != nil {
		errorMap["error"] = err
		json.NewEncoder(w).Encode(errorMap)

		return
	}

	err = deletedAccount.Delete(deletedAccount.AccountNumber)
	if err != nil {
		if err == models.ErrResourceNotFound {
			errorMap["account_number"] = err.Error()
			utils.JSONError(w, errorMap, http.StatusBadRequest)
		} else {
			utils.JSONError(w, nil, http.StatusInternalServerError)
		}

		return
	}

	response := map[string]interface{}{
		"uuid":   uuid,
		"object": "account",
		"delete": true,
	}
	json.NewEncoder(w).Encode(response)

}
