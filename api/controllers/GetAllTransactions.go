package controllers

import (
	"encoding/json"
	"github.com/gaurav/expense_tracker/api/models"
	"github.com/gaurav/expense_tracker/api/serializers"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func GetAllTransactions(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	parameters := mux.Vars(r)
	accountId, _ := strconv.Atoi(parameters["accountId"])

	transactionInstance := models.Transaction{}
	errorMap := make(map[string]interface{})

	allTransactions, err := transactionInstance.All(accountId)
	if err != nil {
		errorMap["error"] = err
		json.NewEncoder(w).Encode(errorMap)

		return
	}

	// Passing last added transaction to serializer
	transactionSerializerInstance := serializers.TransactionSerializer{
		Transactions: allTransactions,
		Many:         true,
	}

	serializedData := transactionSerializerInstance.Serialize()["data"]
	json.NewEncoder(w).Encode(serializedData)
}
