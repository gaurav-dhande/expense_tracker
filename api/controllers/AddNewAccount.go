package controllers

import (
	"encoding/json"
	"github.com/gaurav/expense_tracker/api/models"
	"github.com/gaurav/expense_tracker/api/serializers"
	"github.com/gaurav/expense_tracker/utils"
	"net/http"
)

func AddNewAccount(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	accountInstance := models.Account{}
	errorMap := make(map[string]interface{})

	err := json.NewDecoder(r.Body).Decode(&accountInstance)
	if err != nil {
		errorMap["error"] = err
		json.NewEncoder(w).Encode(errorMap)

		return
	}

	newAccount, err := accountInstance.AddAccount()
	if err != nil {
		if err == models.ErrDuplicateAccountName {
			errorMap["account_name"] = err.Error()
			utils.JSONError(w, errorMap, http.StatusBadRequest)
		} else if err == models.ErrDuplicateAccountNumber {
			errorMap["account_number"] = err.Error()
			utils.JSONError(w, errorMap, http.StatusBadRequest)
		}
		return
	}

	accountSerializerInstance := serializers.AccountSerializer{
		Accounts: []*models.Account{
			newAccount,
		},
		Many: false,
	}

	serializedData := accountSerializerInstance.Serialize()["data"]
	json.NewEncoder(w).Encode(serializedData)
}
