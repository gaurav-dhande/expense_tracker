package controllers

import (
	"encoding/json"
	"github.com/gaurav/expense_tracker/api/models"
	"github.com/gaurav/expense_tracker/api/serializers"
	"net/http"
)

func GetAllAccounts(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	errorMap := make(map[string]interface{})

	accountsFromDb, err := models.GetAllAccounts()
	if err != nil {
		errorMap["error"] = err
		json.NewEncoder(w).Encode(errorMap)

		return
	}

	accountSerializerInstance := serializers.AccountSerializer{
		Accounts: accountsFromDb,
		Many:     true,
	}

	serializedData := accountSerializerInstance.Serialize()["data"]
	json.NewEncoder(w).Encode(serializedData)
}
