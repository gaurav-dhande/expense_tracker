package controllers

import (
	"encoding/json"
	"github.com/gaurav/expense_tracker/api/models"
	"github.com/gaurav/expense_tracker/api/serializers"
	"github.com/gorilla/mux"
	"net/http"
)

func UpdateTransaction(w http.ResponseWriter, r *http.Request) {
	errorMap := make(map[string]interface{})

	w.Header().Set("Content-Type", "application/json")
	parameters := mux.Vars(r)
	uuid, _ := parameters["uuid"]
	transactionInstance := models.Transaction{}

	transaction, err := transactionInstance.RetrieveByUUID(uuid)

	if err != nil {
		errorMap["error"] = err
		json.NewEncoder(w).Encode(errorMap)

		return
	}

	json.NewDecoder(r.Body).Decode(&transaction)
	updatedTransaction, err := transaction.Update(uuid)

	if err != nil {
		errorMap["error"] = err
		json.NewEncoder(w).Encode(errorMap)

		return
	}

	transactionSerializerInstance := serializers.TransactionSerializer{
		Transactions: []*models.Transaction{
			updatedTransaction,
		},
		Many: false,
	}

	serializedData := transactionSerializerInstance.Serialize()["data"]
	json.NewEncoder(w).Encode(serializedData)

}
