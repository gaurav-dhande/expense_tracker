package controllers

import (
	"encoding/json"
	"github.com/gaurav/expense_tracker/api/models"
	"github.com/gaurav/expense_tracker/api/serializers"
	"github.com/gaurav/expense_tracker/utils"
	"net/http"
)

func AddNewTransaction(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	transactionInstance := models.Transaction{}
	errorMap := make(map[string]interface{})

	err := json.NewDecoder(r.Body).Decode(&transactionInstance)
	if err != nil {
		utils.JSONError(w, nil, http.StatusInternalServerError)

		return
	}

	newTransaction, err := transactionInstance.Insert()
	if err != nil {
		if err == models.ErrInvalidAccountNumberForTransaction {
			errorMap["account number"] = err.Error()
			utils.JSONError(w, errorMap, http.StatusBadRequest)
		}

		return
	}

	transactionSerializerInstance := serializers.TransactionSerializer{
		Transactions: []*models.Transaction{
			newTransaction,
		},
		Many: false,
	}

	serializedData := transactionSerializerInstance.Serialize()["data"]
	json.NewEncoder(w).Encode(serializedData)
}
