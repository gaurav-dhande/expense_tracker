package models

import "errors"

var (
	ErrDuplicateAccountNumber             = errors.New("account number already exists")
	ErrDuplicateAccountName               = errors.New("account name already exists")
	ErrInvalidAccountNumberForTransaction = errors.New("account number does not exist")
	ErrResourceNotFound                   = errors.New("the specified resource was not found")
)
