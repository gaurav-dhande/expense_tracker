package models

import (
	"database/sql"
	"github.com/gaurav/expense_tracker/cmd"
	"github.com/gaurav/expense_tracker/utils"
)

type Account struct {
	Id             int    `db:"ID" json:"id"`
	UUID           string `db:"UUID" json:"uuid"`
	AccountNumber  int    `db:"ACCOUNT_NUMBER" json:"account_number"`
	AccountName    string `db:"ACCOUNT_NAME" json:"account_name"`
	AccountType    string `db:"ACCOUNT_TYPE" json:"account_type"`
	CurrentBalance int    `db:"CURRENT_BALANCE" json:"current_balance"`
	IsRed          bool   `db:"IS_RED" json:"is_red"`
}

func (a *Account) RetrieveByUUID(uuid string) (*Account, error) {

	fetchQuery := "SELECT ACCOUNT_NUMBER, ACCOUNT_NAME ,ACCOUNT_TYPE, CURRENT_BALANCE, IS_RED FROM ACCOUNTS WHERE UUID = ?"
	err := cmd.DB.Get(a, fetchQuery, uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, err
		}
	}
	return a, nil
}

func (a *Account) CheckIfAccountExists(accountNumber int) bool {
	var existingAccount Account
	accountNumberFetchQuery := "SELECT ACCOUNT_NUMBER, ACCOUNT_NAME FROM ACCOUNTS " +
		"WHERE ACCOUNT_NUMBER=?"

	err := cmd.DB.Get(&existingAccount, accountNumberFetchQuery, accountNumber)
	if err == sql.ErrNoRows {
		return false
	}

	return true
}

func (a *Account) AddAccount() (*Account, error) {
	var existingAccount Account

	if existingAccount.CheckIfAccountExists(a.AccountNumber) {
		return nil, ErrDuplicateAccountNumber
	}

	accountNameFetchQuery := "SELECT ACCOUNT_NAME FROM ACCOUNTS WHERE ACCOUNT_NAME=?"

	err := cmd.DB.Get(&existingAccount, accountNameFetchQuery, a.AccountName)
	if err != sql.ErrNoRows {
		return nil, ErrDuplicateAccountName
	}

GenerateNewUUID:
	uuid := utils.CreateNewUUID()
	_, err = existingAccount.RetrieveByUUID(uuid)
	if err == nil {
		goto GenerateNewUUID
	}

	a.UUID = uuid
	a.CurrentBalance = 0
	a.IsRed = false

	insertQuery := "INSERT INTO ACCOUNTS " +
		"(UUID, ACCOUNT_NUMBER, ACCOUNT_NAME, ACCOUNT_TYPE, CURRENT_BALANCE, IS_RED) " +
		"VALUES (:UUID, :ACCOUNT_NUMBER,  :ACCOUNT_NAME, :ACCOUNT_TYPE, :CURRENT_BALANCE, :IS_RED)"

	tx := cmd.DB.MustBegin()
	result, err := tx.NamedExec(insertQuery, &a)
	if err != nil {
		_ = tx.Rollback()

		return nil, err
	}
	_ = tx.Commit()

	lastInsertedID, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	fetchQuery := "SELECT UUID, ACCOUNT_NUMBER, ACCOUNT_NAME, ACCOUNT_TYPE, CURRENT_BALANCE, IS_RED FROM ACCOUNTS WHERE ID=?"

	var newAccountFromDB Account
	err = cmd.DB.Get(&newAccountFromDB, fetchQuery, int(lastInsertedID))
	if err != nil {
		return nil, err
	}

	return &newAccountFromDB, nil
}

/*func UpdateAccount(amount int, tType string, id int) {
	var balance int
	var isRedFlag int
	fetchQuery := "SELECT AMOUNT FROM ACCOUNTS WHERE ID = ?"
	row, _ := cmd.DB.Query(fetchQuery, id)
	row.Scan(balance)

	if strings.ToLower(tType) == "credit" {
		balance = balance + amount
	} else {
		balance = balance - amount
	}
	if balance >= 0 {
		isRedFlag = 0
	} else {
		isRedFlag = 1
	}

	updateQuery := "UPDATE ACCOUNTS SET CURRENT_BALANCE = ?, IS_RED = ?"
	cmd.DB.Exec(updateQuery, balance, isRedFlag)
}*/

func GetAllAccounts() ([]*Account, error) {
	accountsFromDb := make([]*Account, 0)
	fetchQuery := "SELECT UUID, ACCOUNT_NUMBER, ACCOUNT_NAME, ACCOUNT_TYPE, CURRENT_BALANCE, IS_RED FROM ACCOUNTS"

	err := cmd.DB.Select(&accountsFromDb, fetchQuery)
	if err != nil {
		return nil, err
	}
	return accountsFromDb, nil
}

func (a *Account) Delete(accountNumber int) error {
	var deleteTransactionsInstance Transaction

	err := deleteTransactionsInstance.Delete(a.AccountNumber)
	if err != nil {
		return err
	}

	deleteQuery := "DELETE FROM ACCOUNTS WHERE ACCOUNT_NUMBER = ?"

	res, err := cmd.DB.Exec(deleteQuery, accountNumber)
	if err != nil {
		return err
	}

	numberOfAccountRows, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if numberOfAccountRows == 0 {
		return ErrResourceNotFound
	}

	return nil
}
