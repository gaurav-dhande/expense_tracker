package models

import (
	"database/sql"
	"github.com/gaurav/expense_tracker/cmd"
	"github.com/gaurav/expense_tracker/utils"
	"time"
)

type Transaction struct {
	Id              int    `db:"ID" json:"id"`
	UUID            string `db:"UUID" json:"uuid"`
	AccountNumber   int    `db:"ACCOUNT_NUMBER" json:"account_number"`
	TransactionId   string `db:"TRANSACTION_ID" json:"transaction_id"`
	TransactionType string `db:"TRANSACTION_TYPE" json:"transaction_type"`
	TransactionTime string `db:"TRANSACTION_TIME" json:"transaction_time"`
	UpdatedTime     string `db:"UPDATED_TIME" json:"updated_time"`
	FromTo          string `db:"FROM_TO" json:"from_to"`
	Amount          int    `db:"AMOUNT" json:"amount"`
}

func (t *Transaction) RetrieveByUUID(uuid string) (*Transaction, error) {

	fetchQuery := "SELECT ACCOUNT_NUMBER, TRANSACTION_ID, TRANSACTION_TYPE, TRANSACTION_TIME, UPDATED_TIME, FROM_TO, AMOUNT " +
		"FROM TRANSACTIONS WHERE UUID = ?"
	err := cmd.DB.Get(t, fetchQuery, uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, err
		}
	}
	return t, nil
}

func (t *Transaction) Insert() (*Transaction, error) {

	var existingTransaction Transaction
	var existingAccount Account

	if !(existingAccount.CheckIfAccountExists(t.AccountNumber)) {
		return nil, ErrInvalidAccountNumberForTransaction
	}

GenerateNewUUID:
	uuid := utils.CreateNewUUID()
	_, err := existingTransaction.RetrieveByUUID(uuid)
	if err == nil {
		goto GenerateNewUUID
	}

	insertQuery := "INSERT INTO TRANSACTIONS " +
		"(UUID, ACCOUNT_NUMBER, TRANSACTION_ID, TRANSACTION_TYPE, TRANSACTION_TIME, UPDATED_TIME, FROM_TO, AMOUNT) " +
		"VALUES (:UUID, :ACCOUNT_NUMBER, :TRANSACTION_ID, :TRANSACTION_TYPE, :TRANSACTION_TIME, :UPDATED_TIME, :FROM_TO, :AMOUNT)"

	t.UUID = uuid
	t.TransactionTime = time.Now().Format("2006-01-02 15:04:05")
	t.UpdatedTime = time.Now().Format("2006-01-02 15:04:05")

	tx := cmd.DB.MustBegin()
	result, err := tx.NamedExec(insertQuery, &t)
	if err != nil {
		_ = tx.Rollback()

		return nil, err
	}
	_ = tx.Commit()

	lastInsertedId, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	fetchQuery := "SELECT UUID, ACCOUNT_NUMBER, TRANSACTION_ID, TRANSACTION_TYPE, TRANSACTION_TIME, UPDATED_TIME, FROM_TO, AMOUNT " +
		"FROM TRANSACTIONS WHERE ID = ?"
	var newTransactionFromDb Transaction

	err = cmd.DB.Get(&newTransactionFromDb, fetchQuery, int(lastInsertedId))
	if err != nil {
		return nil, err
	}
	return &newTransactionFromDb, nil
}

func (t *Transaction) All(parameters int) ([]*Transaction, error) {
	allTransactions := make([]*Transaction, 0)

	fetchQuery := "SELECT UUID, ACCOUNT_NUMBER, TRANSACTION_ID, TRANSACTION_TYPE, TRANSACTION_TIME, UPDATED_TIME, FROM_TO, AMOUNT " +
		"FROM TRANSACTIONS WHERE ACCOUNT_NUMBER = ?"
	err := cmd.DB.Select(&allTransactions, fetchQuery, parameters)
	if err != nil {
		return nil, err
	}
	return allTransactions, nil
}

func (t *Transaction) Update(uuid string) (*Transaction, error) {

	updateQuery := "UPDATE TRANSACTIONS " +
		"SET ACCOUNT_NUMBER = :ACCOUNT_NUMBER, TRANSACTION_TYPE = :TRANSACTION_TYPE, UPDATED_TIME = :UPDATED_TIME, FROM_TO = :FROM_TO, AMOUNT = :AMOUNT " +
		"WHERE UUID = :UUID ;"

	t.UUID = uuid
	t.UpdatedTime = time.Now().Format("2006-01-02 15:04:05")

	tx := cmd.DB.MustBegin()
	_, err := tx.NamedExec(updateQuery, &t)
	if err != nil {
		_ = tx.Rollback()
		return nil, err
	}
	_ = tx.Commit()

	fetchQuery := "SELECT UUID, ACCOUNT_NUMBER, TRANSACTION_ID, TRANSACTION_TYPE, TRANSACTION_TIME, UPDATED_TIME, FROM_TO, AMOUNT " +
		"FROM TRANSACTIONS WHERE UUID = ?"
	var updatedTransaction Transaction
	err = cmd.DB.Get(&updatedTransaction, fetchQuery, uuid)
	if err != nil {
		return nil, err
	}

	return &updatedTransaction, nil
}

func (t Transaction) Delete(accountNumber int) error {
	deleteQuery := "DELETE TRANSACTIONS FROM TRANSACTIONS LEFT JOIN ACCOUNTS ON ACCOUNTS.ACCOUNT_NUMBER = TRANSACTIONS.ACCOUNT_NUMBER WHERE TRANSACTIONS.ACCOUNT_NUMBER = ?"

	res, err := cmd.DB.Exec(deleteQuery, accountNumber)
	if err != nil {
		return err
	}

	numberOfAccountRows, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if numberOfAccountRows == 0 {
		return ErrResourceNotFound
	}

	return nil
}
