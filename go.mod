module github.com/gaurav/expense_tracker

go 1.17

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.3.0
	github.com/jmoiron/sqlx v1.3.4
)

require github.com/gorilla/mux v1.8.0 // indirect
