package main

import (
	"fmt"
	"github.com/gaurav/expense_tracker/api"
	"github.com/gaurav/expense_tracker/utils"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func main() {
	r := mux.NewRouter()

	utils.ConnectToDatabase()
	utils.DropTables()
	utils.CreateTables()
	api.Routes(r)

	fmt.Println("Sever started at https://localhost:8000")
	log.Fatal(http.ListenAndServe(":8000", r))
}
